/*
 * Copyright 2018—2019 Andrej Shadura
 * SPDX-License-Identifier: GPL-3.0 or Apache-2.0
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) under
 * the terms of the Apache License, Version 2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the license for the specific language governing permissions and
 * limitations under the license.
 */

package me.shadura.escposprint.printservice

import android.content.Context
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.SendChannel

// Constants that indicate the current connection state
sealed class State {
    object Disconnected : State() // we're doing nothing
    object Connecting : State() // now initiating an outgoing connection
    object NeedsPermission : State()
    object Connected : State()  // now connected to a remote device
    data class Failed(val error: String) : State()
}

sealed class CommServiceMsg
class Connect(val response: CompletableDeferred<State>) : CommServiceMsg()
class Disconnect(val response: CompletableDeferred<State>) : CommServiceMsg()
class Write(val data: ByteArray) : CommServiceMsg()

val String.isBluetoothAddress: Boolean
    get() {
        return (this.count {
            it == ':'
        }) == 5
    }

val String.isUsbAddress: Boolean
    get() {
        return (this.count {
            it == ':'
        }) == 1
    }

fun CoroutineScope.commServiceActor(context: Context, address: String): SendChannel<CommServiceMsg> {
    return when {
        address.isBluetoothAddress ->
            bluetoothServiceActor(address)
        address.isUsbAddress ->
            usbServiceActor(context, address)
        else ->
            throw IllegalStateException("wrong address format")
    }
}
