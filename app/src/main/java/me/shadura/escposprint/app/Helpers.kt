/*
 * Copyright 2019 Andrej Shadura
 * SPDX-License-Identifier: GPL-3.0 or Apache-2.0
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) under
 * the terms of the Apache License, Version 2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the license for the specific language governing permissions and
 * limitations under the license.
 */

package me.shadura.escposprint.app

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.net.Uri
import android.view.View
import android.widget.*
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar
import me.shadura.escposprint.L
import java.io.IOException
import java.lang.reflect.InvocationTargetException
import java.net.HttpURLConnection
import java.net.URL
import kotlin.concurrent.thread

fun Spinner.setOnItemSelectedListener(l: (parent: AdapterView<*>, view: View?, position: Int, id: Long) -> Unit) {
    this.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {

        }

        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            l(parent, view, position, id)
        }
    }
}

fun SeekBar.setOnChangeListener(l: (seekBar: SeekBar?, progress: Int, fromUser: Boolean) -> Unit) {
    this.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            if (!fromUser) {
                l(seekBar, progress, false)
            }
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
            seekBar?.run {
                l(this, this.progress, true)
            }
        }
    })
}

fun BluetoothDevice.getNameOrAlias(default: String = "(unnamed)"): String {
    return try {
        (this.javaClass.getMethod("getAlias").invoke(this) as String?)
    } catch (e: NoSuchMethodException) {
        null
    } catch (e: SecurityException) {
        null
    } catch (e: InvocationTargetException) {
        null
    } ?: this.name ?: default
}

fun View.longSnackbar(@StringRes messageRes: Int, f: (Snackbar.() -> Unit)? = null) {
    snackbar(resources.getString(messageRes), Snackbar.LENGTH_LONG, f)
}

fun View.longSnackbar(message: String, f: (Snackbar.() -> Unit)? = null) {
    snackbar(message, Snackbar.LENGTH_LONG, f)
}

fun View.snackbar(@StringRes messageRes: Int, length: Int = Snackbar.LENGTH_LONG, f: (Snackbar.() -> Unit)? = null) {
    snackbar(resources.getString(messageRes), length, f)
}

fun View.snackbar(message: String, length: Int = Snackbar.LENGTH_LONG, f: (Snackbar.() -> Unit)? = null) {
    with(Snackbar.make(this, message, length)) {
        f?.invoke(this)
        show()
    }
}

fun Snackbar.action(@StringRes actionRes: Int, color: Int? = null, listener: (View) -> Unit) {
    action(view.resources.getString(actionRes), color, listener)
}

fun Snackbar.action(action: String, color: Int? = null, listener: (View) -> Unit) {
    setAction(action, listener)
    color?.let { setActionTextColor(color) }
}

fun Context.toast(message: CharSequence): Toast = Toast
        .makeText(this, message, Toast.LENGTH_SHORT)
        .apply {
            show()
        }

fun Context.getUrl(uri: Uri): ByteArray = object {
    @Volatile var downloadedDocument: ByteArray = byteArrayOf()

    fun downloadUri(): ByteArray {
        downloadedDocument = byteArrayOf()
        val downloadThread = thread(start=true) {
            try {
                with(URL(uri.toString()).openConnection() as HttpURLConnection) {
                    downloadedDocument = inputStream.readBytes()
                }
            } catch (e: IOException) {
                L.e("failed to download ${uri.toString()}", e)
            }
        }
        downloadThread.join()
        L.i("downloaded ${downloadedDocument.size} bytes from ${uri.toString()}")
        return downloadedDocument
    }
}.downloadUri()
