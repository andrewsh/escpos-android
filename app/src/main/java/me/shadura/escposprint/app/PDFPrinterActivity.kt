/*
 * Copyright 2022 Andrej Shadura
 * SPDX-License-Identifier: GPL-3.0 or Apache-2.0
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) under
 * the terms of the Apache License, Version 2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the license for the specific language governing permissions and
 * limitations under the license.
 */

package me.shadura.escposprint.app

import android.content.*
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.*
import android.print.*
import android.widget.TextView
import androidx.annotation.RequiresApi
import kotlinx.coroutines.*
import me.shadura.escposprint.*
import me.shadura.escposprint.printservice.*
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import kotlin.concurrent.thread

class PDFPrinterActivity : AppCompatActivity(), CoroutineScope by MainScope() {
    class PDFDocumentAdapter(private val stream: InputStream, private val name: String) : PrintDocumentAdapter() {
        override fun onLayout(
            oldAttributes: PrintAttributes?,
            newAttributes: PrintAttributes?,
            cancellationSignal: CancellationSignal,
            callback: LayoutResultCallback,
            extras: Bundle?
        ) {
            if (cancellationSignal.isCanceled) {
                callback.onLayoutCancelled()
                return
            }

            val info = PrintDocumentInfo.Builder(name)
                .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
                .setPageCount(PrintDocumentInfo.PAGE_COUNT_UNKNOWN)
                .build()

            callback.onLayoutFinished(info, oldAttributes != newAttributes)
        }

        override fun onWrite(
            pages: Array<out PageRange>,
            destination: ParcelFileDescriptor,
            cancellationSignal: CancellationSignal,
            callback: WriteResultCallback
        ) {
            var outputStream: OutputStream? = null

            try {
                outputStream = FileOutputStream(destination.fileDescriptor)
                stream.copyTo(outputStream)

                if (cancellationSignal.isCanceled) {
                    callback.onWriteCancelled()
                } else {
                    callback.onWriteFinished(arrayOf(PageRange.ALL_PAGES))
                }
            } catch (ex: Exception) {
                callback.onWriteFailed(ex.message)
                L.e("Could not write: ${ex.localizedMessage}")
            } finally {
                stream.close()
                outputStream?.close()
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdfprinter)

        intent?.data?.also { uri: Uri ->
            var uri = uri
            var interactive = false

            if (uri.scheme == "me.shadura.escposprint" && uri.host == "printdocument") {
                interactive = uri.getBooleanQueryParameter("interactive", false)
                uri.getQueryParameter("url")?.also { new_url ->
                    uri = Uri.parse(new_url)
                }
            }

            val prefs = getSharedPreferences(Config.SHARED_PREFS_PRINTERS, Context.MODE_PRIVATE)
            val config = Config.read(prefs)
            if (config.configuredPrinters.size > 1 || interactive) {
                val printManager = getSystemService(Context.PRINT_SERVICE) as PrintManager
                val jobName = "${getString(R.string.app_name)} Document"

                val stream = when {
                    uri.scheme == "http" || uri.scheme == "https" -> {
                        val downloadedDocument = getUrl(uri)
                        if (downloadedDocument.isEmpty()) {
                            return@also
                        }
                        downloadedDocument.inputStream()
                    }
                    else -> {
                        try {
                            contentResolver.openInputStream(uri)
                        } catch (e: IOException) {
                            L.e("failed to open ${uri.toString()}", e)
                            return
                        }
                    }
                }
                val adapter = stream?.let { stream ->
                    PDFDocumentAdapter(stream, uri.path ?: "(unnamed file)")
                }
                if (adapter == null) {
                    L.e("adapter is null!")
                    return
                }
                val attributes = PrintAttributes.Builder().build()

                val job = printManager.print(jobName, adapter, attributes)

                val statusLabel = findViewById<TextView>(R.id.printing_status_label)
                launch {
                    statusLabel.text = getString(R.string.preparing_print_job)
                    while (true) {
                        delay(500)
                        when (job.info.state) {
                            PrintJobInfo.STATE_QUEUED -> {
                                L.d("job queued")
                            }
                            PrintJobInfo.STATE_STARTED -> {
                                L.d("job started")
                                statusLabel.text = getString(R.string.printing)
                                delay(500)
                                moveTaskToBack(true)
                                finish()
                                return@launch
                            }
                            PrintJobInfo.STATE_CANCELED -> {
                                L.d("job cancelled")
                                statusLabel.text = getString(R.string.print_job_cancelled)
                                delay(500)
                                moveTaskToBack(true)
                                finish()
                                return@launch
                            }
                            PrintJobInfo.STATE_FAILED -> {
                                L.d("job failed")
                                statusLabel.text = getString(R.string.print_job_failed)
                                delay(500)
                                moveTaskToBack(true)
                                finish()
                                return@launch
                            }
                            else -> {
                                L.d("job state: ${job.info.state}")
                            }
                        }
                    }
                }
            } else if (config.configuredPrinters.size == 1) {
                val address = config.configuredPrinters.keys.first()
                L.d("using printer $address")

                val intent = Intent(EscPosService.PRINT_ACTION, uri, this, EscPosService::class.java)
                intent.putExtra("address", address)
                startService(intent)

                val statusLabel = findViewById<TextView>(R.id.printing_status_label)
                launch {
                    statusLabel.text = getString(R.string.printing)
                    L.d("started!")
                    moveTaskToBack(true)
                    finish()
                }
            } else {
                L.e("no printers configured, cannot print!")
                toast("No printers configured!")
                finish()
            }
        }
    }
}
