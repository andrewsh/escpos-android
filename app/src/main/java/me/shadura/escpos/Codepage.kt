/*
 * Copyright 2018—2022 Andrej Shadura
 * SPDX-License-Identifier: GPL-3.0-or-later or Apache-2.0
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later
 * version, or under the terms of the Apache License, Version 2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the license for the specific language governing permissions and
 * limitations under the license.
 */

package me.shadura.escpos

import java.nio.charset.Charset

open class Codepage(val name: String) {
    val mapping by lazy {
        ByteArray(128) {
            (it + 128).toByte()
        }.toString(Charset.forName(name)).asSequence().mapIndexed {
            i, v -> v to (i+128).toByte()
        }.toMap()
    }

    open fun canEncode(c: Char): Boolean =
            when {
                c < 128.toChar() ->
                    true
                c in mapping ->
                    true
                else ->
                    false
            }

    fun encode(c: Char): Byte =
            when {
                c < 128.toChar() ->
                    c.code.toByte()
                c in mapping ->
                    mapping.getValue(c)
                else -> 0
            }
}

class IncompleteCodepage(name: String, val missingCharacters: List<Char>) : Codepage(name) {
    override fun canEncode(c: Char): Boolean {
        if (c in missingCharacters) {
            return false
        }
        return super.canEncode(c)
    }
}
